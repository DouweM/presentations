![inline](logo.png)

# *GitLab*

---

## *Job van der Voort*
### VP of Product
### @Jobvo

---

# *Code, Test, Deploy*

---

#[fit] *On your own server*
#[fit] Or on GitLab.com (unlimited, completely free)

---

# [fit] *__History of GitLab__*

---

#[fit] 2011: Open Source
### *Started by Dmitriy, who didn't have running water*

---

#[fit] 2013: GitLab.com
### *Founded by Sytse*
#### (already had running water)

---

> I want to work on GitLab full time
-- Dmitriy, still without running water

---

#[fit] 2014: Founding of GitLab BV
### *5 engineers, 1 sales*

![](founding.jpg)

---

### 2015
# *Y Combinator*
![](team_yc.jpg)

---

#[fit] 2015: $1.5M Seed Round

---

#[fit] 2015: $4M A Round
###[fit] *Khosla, Ashton Kutcher, Joe Montana, Michael Dell*
![](money.jpg)

---

## _> 150,000_ organisations
## 1000 contributors

---

## *~50 employees*

![](team_now.jpg)

---

#[fit] *__How we make $$$__*

---

# Open core

---

![inline](open core.png)

---

![inline](open core 2.png)

---

## Majority of customers starts with Community Edition

---

# *Apple, NASA, SpaceX, IBM, Cisco, Disney*

---

#[fit] *Don't underestimate sales*

![](sales.jpg)

---

#[fit] *Don't underestimate marketing*

![](marketing.jpg)

---

# [fit] *__How we build GitLab__*

---


### *Remote First*

![](team.png)

---

#[fit] *Everyone* has to use Git and GitLab

---

# We're part of a big community

---

### (Almost)
# *Everything we do is open*

---

#[fit] Everything on GitLab.com

---

# Support

![inline](support.png)

---

# Development

![inline](dev.png)

---

# Company

![inline](company.png)

---

# [fit] __*The Secret Sauce*__

---

#[fit] Release _ALWAYS_ on the 22nd of the month
#### Minor (8.2 to 8.3) or major release (8.3 to 9.0)

---

> What did you do for fun yesterday?
-- daily call at 4.30PM UTC

---

![inline](daily.png)

![inline](daily2.png)

---

###[fit] To work asynchronous / remote / like a boss
#[fit] *Write everything down*

---

# Whenever, wherever, however

---

#[fit] E.g. go to the gym in the middle of the day
#[fit] or take a day off to play Fallout 4

---

# *Trust people* avoid process

---

#[fit] Keep your git flows simple
#[fit] *Avoid permission management*

---

#[fit] *New Features driven by excited people*
##[fit] Customers, community, colleagues

---

#[fit] *Most feature requests are ..not great*
###[fit] But a great indication of an unsolved issue

---

## Focus on solving issues
###[fit] *Avoid bloat*

---

#[fit] *community is everything*

---

# *__Future__*


---

#[fit] about.gitlab.com/direction

---

#[fit] GitLab wants to become the one-stop-shop for all media

---

# Near future

- Zero-setup CI (Autoscaling, templates)
- More powerful IDE
- Full featured issue tracker (issue weight, velocity tracking)
- Productivity improvements (notification center, actionable insights)
- Zero-setup deploys (connections to cloud providers, Docker)
- Everything in the UI

---

# Future

- Review and Merge other media
- Chat client
- Chat operations
- Scrum board
- Code quality checks
- More HA stuff

---

# Company

- Grow to 200+ people
- Raise B Round
- Open branches besides USA and NL
- Expand into Asia
- Keep building awesome stuff

---

# Questions?
### *Follow me on twitter: @jobvo*
#### + WE'RE HIRING

---

# If it's not documented, it doesn't exist

---
